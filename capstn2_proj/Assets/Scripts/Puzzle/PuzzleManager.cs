﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    #region Singleton
    public static PuzzleManager instance;

    private void Awake()
    {
        _activePuzzles = new List<Puzzle>();
        instance = this;
    }
    #endregion

    [SerializeField] private List<Puzzle> _activePuzzles;

    private void Start()
    {
        FindAllPuzzles();
    }

    private void FindAllPuzzles()
    {
        var puzzles = FindObjectsOfType<Puzzle>();
        for (int i = 0; i < puzzles.Length; i++)
        {
            _activePuzzles.Add(puzzles[i]);
        }
    }

    public void CompletedTask(Puzzle puzzle)
    {
        puzzle.CompletedTask();
    }

    public void CompletedAllTasks(Puzzle puzzle)
    {
        puzzle.CompletedAllTasks();
    }
}
