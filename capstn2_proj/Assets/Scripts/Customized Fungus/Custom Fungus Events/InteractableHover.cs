﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

/// <summary>
/// The block will execute when the user hovers over an interactable / pickup.
/// </summary>
[EventHandlerInfo("Interactable",
    "Interactable Hovered",
    "The block will execute when the user hovers over an interactable / pickup.")]
[AddComponentMenu("")]
public class InteractableHover : EventHandler
{
    public class InteractableHoverEvent
    {
        public Interactable interactable;
        public InteractableHoverEvent(Interactable nInteractable)
        {
            interactable = nInteractable;
        }
    }

    [SerializeField] private TMPro.TextMeshProUGUI _interactableLabelOverPlayer;

    protected EventDispatcher eventDispatcher;
    
    protected virtual void OnEnable()
    {
        eventDispatcher = FungusManager.Instance.EventDispatcher;
        eventDispatcher.AddListener<InteractableHoverEvent>(OnInteractableHoverEvent);
    }

    protected virtual void OnDisable()
    {
        eventDispatcher.RemoveListener<InteractableHoverEvent>(OnInteractableHoverEvent);
        eventDispatcher = null;
    }

    void OnInteractableHoverEvent(InteractableHoverEvent evt)
    {
        OnInteractableHover(evt.interactable);
    }

    #region Public Members
    public virtual void OnInteractableHover(Interactable interactable)
    {
        Debug.Log("Hovering over: " + interactable.data.name);
        if (_interactableLabelOverPlayer != null)
            _interactableLabelOverPlayer.text = interactable.data.name;
    }

    public override string GetSummary()
    {
        string summary = "";

        return summary;
    }

    #endregion
}
