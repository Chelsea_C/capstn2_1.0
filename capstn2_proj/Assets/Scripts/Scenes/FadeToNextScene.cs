﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeToNextScene : MonoBehaviour
{
    [SerializeField] private string _sceneName;
    [SerializeField] private Color _loadColor;
    [SerializeField] private float _fadeTime = 1.0f;
    public void GoFade()
    {
        Initiate.Fade(_sceneName, _loadColor, _fadeTime);
    }
}
