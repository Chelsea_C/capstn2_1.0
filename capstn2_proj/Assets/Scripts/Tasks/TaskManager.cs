﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskManager : MonoBehaviour
{
    #region Singleton
    public static TaskManager instance;

    void Awake()
    {
        tasks = new List<Task>();
        instance = this;
    }
    #endregion

    public List<Task> tasks;

    Task currentTask;
    int currentIndex;

    void Start()
    {
        currentIndex = 0;
        GetTasks();
        currentTask = tasks[currentIndex];
    }

    void Update()
    {

        if (currentTask != null)
            currentTask.CheckIfHappening();
    }

    public void CompletedTask()
    {
        currentIndex++;
        SetNextTask(currentIndex);

    }

    public void SetNextTask(int currentOrder)
    {
        currentTask = GetTaskByOrder(currentOrder);

        if (currentTask == null)
        {
            CompletedAllTasks();
            return;
        }
    }

    public virtual void CompletedAllTasks()
    {
        Debug.Log(name + ": Completed all tasks");
    }

    public Task GetTaskByOrder(int index)
    {
        if (index < tasks.Count)
            return tasks[index];
        return null;
    }

    public void GetTasks()
    {
        tasks = new List<Task>();
        Task[] _tasks = GetComponentsInChildren<Task>();
        for (int i = 0; i < _tasks.Length; i++)
        {
            tasks.Add(_tasks[i]);
        }
    }

}