﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBounds : MonoBehaviour
{
    [SerializeField] private Collider2D _boundary;
    [SerializeField] private GameObject _player;
    
    private float screenAspect;
    private float camHalfHeight;
    private float camHalfWidth;
    private float xMax;
    private float xMin;
    private float yMax;
    private float yMin;

    private void Update()
    {
        Clamp();
    }

    private void Clamp()
    {
        // In case camera bounds are taller than map bounds
        if (Camera.main.orthographicSize * 2 > _boundary.bounds.size.y)
        {
            Camera.main.orthographicSize = _boundary.bounds.size.y / 2;
        }

        screenAspect = (float)Screen.width / (float)Screen.height;
        camHalfHeight = Camera.main.orthographicSize;
        camHalfWidth = screenAspect * camHalfHeight;

        // In case camera bounds are wider than map bounds
        if (camHalfWidth > _boundary.bounds.size.x / 2)
        {
            camHalfWidth = _boundary.bounds.size.x / 2;
        }

        xMax = _boundary.bounds.max.x - camHalfWidth;
        xMin = _boundary.bounds.min.x + camHalfWidth;
        yMax = _boundary.bounds.max.y - camHalfHeight;
        yMin = _boundary.bounds.min.y + camHalfHeight;
        float z = transform.position.z;

        transform.position = new Vector3 (
            Mathf.Clamp(transform.position.x, xMin, xMax),
            Mathf.Clamp(transform.position.y, yMin, yMax),
            z);
    }
}
