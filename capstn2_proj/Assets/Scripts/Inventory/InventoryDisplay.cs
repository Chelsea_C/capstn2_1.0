﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Fungus;

public class InventoryDisplay : MonoBehaviour
{
    private void Awake()
    {
        
    }

    [SerializeField] private InventorySlot[] _slots;
    [SerializeField] private PlayerData _data;

    public InventorySlot[] slots { get { return _slots; } }

    void Start()
    {
        _slots = GetComponentsInChildren<InventorySlot>();
        FungusManager.Instance.EventDispatcher.AddListener<PickUpInteractable.PickUpInteractableEvent>(UpdateInventorySlots);
    }

    private void Update()
    {
        
    }


    public void UpdateInventorySlots(PickUpInteractable.PickUpInteractableEvent evt)
    {

        Debug.Log("[InventoryDisplay] Updating inventory.");
        //if (_data.inventory.Count <= 0)
        //    return;

        //for(int i = 0; i < _slots.Length; i++)
        //{
        //    for(int j = 0; j < _data.maxInventoryCount; j++)
        //    {
        //        if (_data.inventory[i] == null)
        //            return;

        //        _slots[i].item = _data.inventory[i];
        //    }
        //}

        for(int i = 0; i < _data.inventory.Count; i++)
        {
            if (i > _slots.Length)
                return;

            for(int j = 0; j < _slots.Length; j++)
            {
                if (_data.inventory[i] == null)
                    return;

                _slots[j].item = _data.inventory[j];
            }
        }
    }
}
