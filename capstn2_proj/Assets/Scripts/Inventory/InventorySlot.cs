﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Image))]
public class InventorySlot : MonoBehaviour
{
    private Image _img;
    private Sprite _emptySprite;
    [SerializeField] private InteractableData _item;

    public InteractableData item { get { return _item; } set { _item = value; } }

    private void Start()
    {
        _img = GetComponent<Image>();
        _emptySprite = _img.sprite;
    }

    private void Update()
    {
        if (_item != null)
            _img.sprite = _item.icon;
        else
            _img.sprite = _emptySprite;
    }

    public void AddItem(Pickup pickup)
    {
        _item = pickup.data;
    }

    private void RemoveItem()
    {
        _item = null;
    }
}
