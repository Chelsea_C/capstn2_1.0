﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI), typeof(Button))]
public class InteractableLabel : MonoBehaviour
{
    private TextMeshProUGUI _text;
    private Button _button;
    private Interactable _interactable;

    private void Start()
    {
        _interactable = GetComponentInParent<Interactable>();
        _text = GetComponent<TextMeshProUGUI>();
        _button = GetComponent<Button>();

        Initialize();
    }

    private void Update()
    {
        
    }

    private void Initialize()
    {
        _text.text = _interactable.data.labelText;
    }

    public void SetActive()
    {

    }
}
