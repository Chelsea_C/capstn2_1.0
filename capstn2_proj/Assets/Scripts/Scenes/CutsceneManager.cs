﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneManager : MonoBehaviour
{
    //[System.Serializable]
    //public struct AnimObject
    //{
    //    public GameObject container;
    //    public float timeUntilNext;
    //    public bool disableOnEnd;
    //}

    //[System.Serializable]
    //public struct AudioPlayer
    //{
    //    public AudioSource container;
    //    public float timeUntilNext;
    //    public bool isLoop;
    //}

    //[SerializeField] private AnimObject[] _animationHolders;
    //[SerializeField] private AudioPlayer[] _audioPlayers;

    //[SerializeField] private AnimObject _currentAnimObj;
    //[SerializeField] private int _index = 0;
    //[SerializeField] private bool _isAnimating = false;


    public static CutsceneManager instance;
    private void Awake()
    {
        instance = this;
    }

    [System.Serializable]
    public class ReferenceObject
    {
        public int triggerID;
        public GameObject[] objects;
    }

    [SerializeField] private ReferenceObject[] _animatedObjects;

    [Header("Audio")]
    [SerializeField] private AudioPlayer _audioPlayer;
    [SerializeField] private AudioClip[] _sfxClips;
    [SerializeField] private AudioClip[] _voiceLines;
    [SerializeField] private AudioClip _bgm;

    [Tooltip("AudioPlayer Default: 0.20")]
    [SerializeField] private float _bgm_volume;
    [Tooltip("AudioPlayer Default: 1")]
    [SerializeField] private float _bgm_pitch;

    [SerializeField] private AudioClip _ambience;
    [Tooltip("AudioPlayer Default: 0.20")]
    [SerializeField] private float _ambience_volume;
    [Tooltip("AudioPlayer Default: 1")]
    [SerializeField] private float _ambience_pitch;
    [SerializeField] private int _voiceLineIndex = -1;

    [Header("State")]
    [SerializeField] private bool _hasStarted = false;

    [Header("Scene Fader")]
    [SerializeField] private string _nextScene;
    [SerializeField] private Color _loadColor = Color.black;
    [SerializeField] private float _fadeTime = 5.0f;

    [Header("Debugging")]
    [SerializeField] private bool showDebugLog = true;

    public GameObject lastObjectClicked;

    public event Action OnCutsceneStart;
    public event Action<int> OnTriggerEvent;
    public event Action OnCutsceneEnd;
    

    void Start()
    {
        //_index = 0;
        //_currentAnimObj = _animationHolders[_index];

        //foreach (AnimObject animObj in _animationHolders)
        //    animObj.container.SetActive(false);

        OnCutsceneEnd += EndCutscene;
        OnCutsceneStart += StartCutscene;
        OnTriggerEvent += TriggerEvent;

        _audioPlayer = FindObjectOfType<AudioPlayer>();
        if (_audioPlayer == null)
            Debug.LogError("[CutsceneManager] No audioplayer found in scene.");

    }

    void Update()
    {
        //Debugging
        if (Input.GetKeyDown(KeyCode.Space))
            EndCutscene();

    }

    public void StartCutscene()
    {
        if (_hasStarted)
            return;

        _hasStarted = true;
        _voiceLineIndex = -1;

        if (showDebugLog)
            Debug.Log("[CutsceneManager] Starting cutscene.");

        foreach (ReferenceObject ro in _animatedObjects)
        {
            foreach (GameObject obj in ro.objects)
            {
                obj.SetActive(false);
                if (showDebugLog)
                    Debug.Log("[CutsceneManager] Animated object successfully hidden.");
            }
        }

        if (showDebugLog)
            Debug.Log("[CutsceneManager] Initializing audio.");

        _audioPlayer.PlayBGM(_bgm, _bgm_volume, _bgm_pitch);
        _audioPlayer.PlayAmbience(_ambience, _ambience_volume, _ambience_pitch);

        TriggerNextVoiceLine();
        Debug.Log("[CutsceneManager] Queuing first voiceline");
    }

    public void TriggerEvent(int id)
    {
        if (showDebugLog)
            Debug.Log("[CutsceneManager] Triggering event ID: " + id);

        foreach (GameObject obj in _animatedObjects[id].objects)
        {
            obj.SetActive(true);
        }
    }

    public void TriggerSFX(int id)
    {
        if (showDebugLog)
            Debug.Log("[CutsceneManager] Triggering SFX ID: " + id);

        _audioPlayer.PlaySFX(_sfxClips[id]);

    }

    public void TriggerNextVoiceLine()
    {
        if (_voiceLineIndex >= _voiceLines.Length - 1)
            return;

        if (showDebugLog)
            Debug.Log("[CutsceneManager] Triggering voice line ID: " + (_voiceLineIndex +1));

        _voiceLineIndex++;
        _audioPlayer.PlayVoiceLine(_voiceLines[_voiceLineIndex]);
    }

    public void TriggerVoiceLine(int id)
    {
        if (id >= _voiceLines.Length - 1)
            return;

        if (showDebugLog)
            Debug.Log("[CutsceneManager] Triggering voice line ID: " + id);
        
        _audioPlayer.PlayVoiceLine(_voiceLines[id]);
    }

    public void QueueVoiceLine(int id)
    {
        _audioPlayer.QueueVoiceLine(_voiceLines[id]);
    }

    public void EndCutscene()
    {
        if(showDebugLog)
            Debug.Log("[CutsceneManager] Ending cutscene.");

        Initiate.Fade(_nextScene, _loadColor, _fadeTime);
    }

    public void EnableObject(GameObject go)
    {
        go.SetActive(true);
    }

    public void DisableObject(GameObject go)
    {
        go.SetActive(false);
    }

}
