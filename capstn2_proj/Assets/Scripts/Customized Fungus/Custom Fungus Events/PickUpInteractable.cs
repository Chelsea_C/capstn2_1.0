﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

/// <summary>
/// The block will execute when the player picks up an interactable.
/// </summary>
[EventHandlerInfo("Interactable",
    "Pick Up Interactable",
    "The block will execute when the player clicks on a pick up and picks up the interactable.")]
[AddComponentMenu("")]
public class PickUpInteractable : EventHandler
{

    public class PickUpInteractableEvent
    {
        public Pickup pickup;
        public PickUpInteractableEvent(Pickup nPickup)
        {
            pickup = nPickup;
        }
    }

    [SerializeField] protected Pickup _item;
    protected EventDispatcher _eventDispatcher;
    [SerializeField] protected PlayerData _data;

    protected virtual void OnEnable()
    {
        _eventDispatcher = FungusManager.Instance.EventDispatcher;
        _eventDispatcher.AddListener<PickUpInteractableEvent>(OnPickUpInteractableEvent);
    }

    protected virtual void OnDisable()
    {
        _eventDispatcher.RemoveListener<PickUpInteractableEvent>(OnPickUpInteractableEvent);
        _eventDispatcher = null;
    }

    void OnPickUpInteractableEvent(PickUpInteractableEvent evt)
    {
        OnPickUpInteractableEvent(evt.pickup);
    }

    #region Public Members

    public void OnPickUpInteractableEvent(Pickup nPickup)
    {
        _data.AddItemToInventory(nPickup);
        Debug.Log("Picked up: " + nPickup.data.name);
        nPickup.gameObject.SetActive(false);
    }

    #endregion

}
