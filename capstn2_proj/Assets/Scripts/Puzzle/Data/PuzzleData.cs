﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class PuzzleData : ScriptableObject
{
    [SerializeField] private PuzzleTypeEnum _type;
    public PuzzleTypeEnum type { get { return _type; } }
}
