﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchSlots : PuzzleStep
{
    [SerializeField] private List<PuzzleSlot> _slots;
    [SerializeField] private SlotsData _data;

    private Puzzle puzzle;

    private void Start()
    {
        _slots = new List<PuzzleSlot>();

        puzzle = GetComponentInParent<Puzzle>();
        if (puzzle == null)
            Debug.LogError("[PuzzleStep][" + name + "]: No Puzzle component found in parent.");

        var pSlots = transform.parent.GetComponentsInChildren<PuzzleSlot>();
        foreach (PuzzleSlot slot in pSlots)
            _slots.Add(slot);
    }

    public override void CheckIfHappening()
    {
        if (_data.CheckSolution(_slots))
        {
            Debug.Log("[Puzzle][MatchSlots] Completed!");

            foreach (PuzzleSlot ps in _slots)
                ps.isLocked = true;

            PuzzleManager.instance.CompletedTask(puzzle);
        }
    }



}
