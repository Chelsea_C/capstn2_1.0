﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

/// <summary>
/// The block will execute when the user hovers over an interactable / pickup.
/// </summary>
[EventHandlerInfo("Interactable",
    "Exit Interactable Hover",
    "The block will execute when the mouse stops hovering over an interactable / pickup.")]
[AddComponentMenu("")]
public class ExitInteractableHover : EventHandler
{
    public class ExitInteractableHoverEvent
    {
        public Interactable interactable;
        public ExitInteractableHoverEvent()
        {
            
        }
    }

    [SerializeField] private TMPro.TextMeshProUGUI _interactableLabelOverPlayer;

    protected EventDispatcher eventDispatcher;

    protected virtual void OnEnable()
    {
        eventDispatcher = FungusManager.Instance.EventDispatcher;
        eventDispatcher.AddListener<ExitInteractableHoverEvent>(OnExitInteractableHoverEvent);
    }

    protected virtual void OnDisable()
    {
        eventDispatcher.RemoveListener<ExitInteractableHoverEvent>(OnExitInteractableHoverEvent);
        eventDispatcher = null;
    }

    void OnExitInteractableHoverEvent(ExitInteractableHoverEvent evt)
    {
        OnExitInteractableHover();
    }

    #region Public Members
    public virtual void OnExitInteractableHover()
    {
        if (_interactableLabelOverPlayer != null)
            _interactableLabelOverPlayer.text = "";
    }

    public override string GetSummary()
    {
        string summary = "";

        return summary;
    }

    #endregion
}
