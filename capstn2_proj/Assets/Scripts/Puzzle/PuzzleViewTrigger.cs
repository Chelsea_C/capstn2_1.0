﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleViewTrigger : MonoBehaviour
{
    [SerializeField] private Puzzle _puzzle;

    public void OnMouseDown()
    {
        Debug.Log("[PuzzleViewTrigger] " + name + ": Switching cameras.");
        _puzzle.MoveToPuzzleView();
    }

}
