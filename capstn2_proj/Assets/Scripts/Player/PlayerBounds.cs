﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBounds : MonoBehaviour
{
    [SerializeField] private BoxCollider2D boundary;
    [SerializeField] private float _edgeThickness = 1f;

    public float xMax { get { return boundary.bounds.max.x - _edgeThickness; } }
    public float xMin { get { return boundary.bounds.min.x + _edgeThickness; } }

    private PlayerActor player;

    [SerializeField] private float _xMax;
    [SerializeField] private float _xMin;

    private void Start()
    {
        if (boundary == null)
            Debug.LogError("No boundary assigned for player bounds.");

        player = GetComponent<PlayerActor>();
    }

    private void Update()
    {
        Clamp();
    }

    private void Clamp()
    {
        float xMax = boundary.bounds.max.x - _edgeThickness;
        float xMin = boundary.bounds.min.x + _edgeThickness;

        _xMax = xMax;
        _xMin = xMin;
        
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, xMin, xMax),
            transform.position.y,
            transform.position.z);
    }
}
