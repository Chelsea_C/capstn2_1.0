﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player Data", menuName = "ScriptableObjects/PlayerData")]
public class PlayerData : ScriptableObject
{
    [SerializeField] private List<InteractableData> _inventory;
    [SerializeField] private int _maxInventoryCount;

    public List<InteractableData> inventory { get { return _inventory; } }
    public int maxInventoryCount { get { return _maxInventoryCount; } }

    public void Initialize()
    {
        _inventory = new List<InteractableData>();
    }

    public void AddItemToInventory(Pickup pickup)
    {
        if(_inventory.Count < _maxInventoryCount)
            _inventory.Add(pickup.data);
    }

    public void RemoveItemFromInventory(Pickup pickup)
    {
        if(_inventory.Contains(pickup.data))
            _inventory.Remove(pickup.data);
    }
}