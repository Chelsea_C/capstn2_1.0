﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New PuzzleData", menuName = "ScriptableObjects/Puzzle/SlotsData")]
public class SlotsData : PuzzleData
{
    [SerializeField] private bool _orderMatters = true;
    public bool orderMatters { get { return _orderMatters; } }

    [SerializeField] private List<Sprite> _solution;
    public List<Sprite> solution { get { return _solution; } }

    public int slotsCount { get { return _solution.Count; } }

    public bool CheckSolution(List<PuzzleSlot> answer)
    {
        if (answer.Count == slotsCount)
        {
            if (orderMatters)
            {
                for (int i = 0; i < slotsCount; i++)
                {
                    if (_solution[i] != answer[i].sprite)
                        return false;
                }
                return true;
            }
            else
            {
                var temp = new List<Sprite>();
                foreach(PuzzleSlot ps in answer)
                {
                    temp.Add(ps.sprite);
                }

                for (int i = 0; i < slotsCount; i++)
                {
                    // If answer contains part of the solution, remove it
                    if (temp.Contains(_solution[i]))
                        temp.RemoveAt(i);
                }

                if (temp.Count == 0)
                    return true;
                else
                    return false;
            }
        }
        else
            Debug.Log("[SlotsData] Answer does not have correct number of slots for solution.");
        return false;
    }
}
