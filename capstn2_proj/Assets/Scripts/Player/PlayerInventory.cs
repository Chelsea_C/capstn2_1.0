﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    #region Singleton
    public static PlayerInventory instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    [SerializeField] private PlayerData _data;

    public List<InteractableData> items { get { return _data.inventory; } }
    public int maxItemCount { get { return _data.maxInventoryCount; } }

    void Start()
    {
        _data.Initialize();
    }
    
    void Update()
    {
        
    }

    public void AddItem(Pickup pickup)
    {
        if (items.Count < maxItemCount)
        {
            _data.AddItemToInventory(pickup);
        }
    }

    public void RemoveItem(Pickup pickup)
    {
        _data.RemoveItemFromInventory(pickup);
    }
}
