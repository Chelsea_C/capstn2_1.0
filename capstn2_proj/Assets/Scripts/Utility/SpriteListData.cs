﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SpriteListData", menuName = "ScriptableObjects/SpriteListData")]
public class SpriteListData : ScriptableObject
{
    [SerializeField] private List<Sprite> _list;
    public List<Sprite> list { get { return _list; } }
}
