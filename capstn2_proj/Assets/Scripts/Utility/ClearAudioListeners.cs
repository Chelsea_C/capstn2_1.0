﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearAudioListeners : MonoBehaviour
{
    private void Awake()
    {
        AudioListener[] aL = FindObjectsOfType<AudioListener>();
        for (int i = 0; i < aL.Length; i++)
        {
            if (i == 0)
                continue;
            DestroyImmediate(aL[i]);
        }
    }
}
