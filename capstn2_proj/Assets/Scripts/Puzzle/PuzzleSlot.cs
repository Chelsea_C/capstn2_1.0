﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class PuzzleSlot : MonoBehaviour
{
    [SerializeField] private SpriteListData _spritePool;

    [SerializeField] private bool _isLocked = false;
    public bool isLocked { get { return _isLocked; } set { _isLocked = value; } }

    [SerializeField] private int _currentIndex;
    public int currentIndex { get { return _currentIndex; } }

    private MatchSlots _puzzle;

    private void Awake()
    {
        _currentIndex = 0;
        //sprite = _sprites.list[_currentIndex];
    }

    private void Start()
    {
        _puzzle = transform.parent.GetComponentInChildren<MatchSlots>();
    }

    private void Update()
    {
        sprite = _spritePool.list[_currentIndex];
    }

    public Sprite sprite
    {
        get
        {
            return GetComponent<SpriteRenderer>().sprite;
        }

        set
        {
            GetComponent<SpriteRenderer>().sprite = value;
        }
    }

    public void LoadNextSprite()
    {
        // if current index is the last, go back to zero
        if (_currentIndex >= _spritePool.list.Count - 1)
            _currentIndex = 0;
        else
            _currentIndex++;

        // sprite = _sprites.list[_currentIndex];
    }

    public void LoadPreviousSprite()
    {
        // if current index is the first,  go to last
        if (_currentIndex == 0)
            _currentIndex = _spritePool.list.Count - 1;
        else
            _currentIndex--;

        // sprite = _sprites.list[_currentIndex];
    }
}
