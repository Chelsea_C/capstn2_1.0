﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Interactable Data", menuName = "ScriptableObjects/Interactable/InteractableData")]
public class InteractableData : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private string _id;
    [SerializeField] private string _labelText;
    [SerializeField] private Sprite _icon;
    [SerializeField] private Vector3 _iconScale;

    public new string name { get { return _name; } }
    public string id { get { return _id; } }
    public string labelText { get { return _labelText; } }
    public Sprite icon { get { return _icon; } }
    public Vector3 iconScale { get { return _iconScale; } }
}
