﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AudioPlayer : MonoBehaviour
{
    public AudioSource sfxPlayer;
    public AudioSource bgm;
    public AudioSource ambience;
    public AudioSource narration;

    [SerializeField] private List<AudioClip> _queuedVoiceLines = new List<AudioClip>();
    [SerializeField] private AudioClip _currentVoiceLine;

    private void Update()
    {
        _currentVoiceLine = narration.clip;
        PlayQueuedVoiceLines();

        Debug.LogError(narration.isPlaying);
    }

    public void PlayBGM(AudioClip music, float volume = 0.2f, float pitch = 1)
    {
        Debug.Log("[AudioPlayer] Attempting to play BGM.");
        // If the same clip is already playing, ignore call
        if (bgm.clip == music && bgm.isPlaying)
        {
            Debug.Log("[AudioPlayer] BGM already playing.");
            return;
        }

        bgm.clip = music;
        bgm.volume = volume;
        bgm.pitch = pitch;

        if (bgm.clip != null)
        {
            if (bgm.isPlaying)
                return;

            bgm.loop = true;
            bgm.Play();
            Debug.Log("[AudioPlayer] Successfully playing BGM.");
        }
        else
            Debug.Log("[AudioPlayer] BGM player has no audio clip set.");
    }

    public void PlayAmbience(AudioClip amb, float volume = 0.2f, float pitch = 1)
    {
        Debug.Log("[AudioPlayer] Attempting to play Ambience.");
        // If the same clip is already playing, ignore call
        if (ambience.clip == amb && ambience.isPlaying)
        {
            Debug.Log("[AudioPlayer] Ambience already playing.");
            return;
        }

        ambience.clip = amb;
        ambience.volume = volume;
        ambience.pitch = pitch;

        if (ambience.clip != null)
        {
            if (ambience.isPlaying)
                return;

            ambience.loop = true;
            ambience.Play();
            Debug.Log("[AudioPlayer] Successfully playing Ambience.");
        }
        else
            Debug.Log("[AudioPlayer] Ambience player has no audio clip set.");
    }

    public void PlaySFX(AudioClip sfx)
    {
        sfxPlayer.PlayOneShot(sfx);
    }

    public void PlayVoiceLine(AudioClip line)
    {            
        
        // if attempting to play the same voice line again, return
        if (narration.clip == line)
            return;

        if (narration.isPlaying)
        {
            // if current voice line is different from new and unqueued voice line, queue the voice line
            if (!_queuedVoiceLines.Contains(line))
            {
                QueueVoiceLine(line);
                return;
            }
        }

        narration.clip = line;
        narration.Play();
        //narration.PlayOneShot(line);
    }

    public void QueueVoiceLine(AudioClip nextLine)
    {
        _queuedVoiceLines.Insert(0, nextLine);
    }

    private void PlayQueuedVoiceLines()
    {
        if(_queuedVoiceLines.Count > 0)
        {
            int lastIndex = _queuedVoiceLines.Count - 1;
            if (_queuedVoiceLines[lastIndex] != _currentVoiceLine && !narration.isPlaying)
            {
                PlayVoiceLine(_queuedVoiceLines[lastIndex]);
                _queuedVoiceLines.RemoveAt(lastIndex);
            }
        }
    }

}
