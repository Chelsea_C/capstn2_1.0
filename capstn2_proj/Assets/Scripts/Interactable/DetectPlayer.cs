﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayer : MonoBehaviour
{
    [SerializeField] private LayerMask _playerMask;
    [SerializeField] private float _detectionRadius;

    public float detectionRadius { get { return _detectionRadius; }  set { _detectionRadius = value; } }
    public bool playerIsDetected = false;

    private void Update()
    {
        CheckForPlayer();
    }

    private void CheckForPlayer()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, _detectionRadius, _playerMask);
        IsPlayerDetected(hitColliders);
        if (hitColliders.Length <= 0)
        {
            return;
        }
    }

    private void IsPlayerDetected(Collider[] colliders)
    {
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject.GetComponent<PlayerActor>() != null)
            {
                playerIsDetected = true;
                return;
            }
        }

        playerIsDetected = false;
    }

    #region Gizmos
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Check that it is being run in Play Mode, so it doesn't try to draw this in Editor mode
        //Draw a cube where the OverlapBox is (positioned where your GameObject is as well as a size)
        //Gizmos.DrawWireCube(transform.position, transform.lossyScale);
        Gizmos.DrawWireSphere(this.transform.position, _detectionRadius);
    }
    #endregion

}
