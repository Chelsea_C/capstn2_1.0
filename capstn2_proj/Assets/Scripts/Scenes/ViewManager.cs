﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ViewManager : MonoBehaviour
{
    #region Singleton
    public static ViewManager instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion

    [SerializeField] private CinemachineVirtualCamera[] _puzzleCams;
    [SerializeField] private CinemachineVirtualCamera _playerCam;
    [SerializeField] private CinemachineVirtualCamera _activeCam;

    [SerializeField] private UnityEngine.UI.Button _backButton;

    public UnityEngine.UI.Button backButton { get { return _backButton; } }
    public CinemachineVirtualCamera[] puzzleCams { get { return _puzzleCams; } }
    public CinemachineVirtualCamera active { get { return _activeCam; } set { _activeCam = value; } }
    public CinemachineVirtualCamera playerCam { get { return _playerCam; } }

    private void Start()
    {
        if (_playerCam != null)
        {
            SetActiveCam(_playerCam);
        }

        if (_backButton != null)
        {
            _backButton.onClick.AddListener(LeavePuzzleView);
        }
    }

    private void Update()
    {
        if (_activeCam != _playerCam)
            _backButton.gameObject.SetActive(true);
        else
            _backButton.gameObject.SetActive(false);
    }


    public void SwitchToCamera(string name)
    {
        for (int i = 0; i < _puzzleCams.Length; i++)
        {
            if (_puzzleCams[i].name == name)
            {
                SetActiveCam(_puzzleCams[i]);
            }
        }
    }

    public void SwitchToCamera(CinemachineVirtualCamera cam)
    {
        for(int i = 0; i < _puzzleCams.Length; i++)
        {
            if(_puzzleCams[i] == cam)
            {
                SetActiveCam(_puzzleCams[i]);
            }
        }
    }

    public void SetActiveCam(CinemachineVirtualCamera cam)
    {
        cam.Priority++;
        if(_activeCam != null)
            _activeCam.Priority--;
        _activeCam = cam;
    }

    public void SwitchToDefault()
    {
        SetActiveCam(_playerCam);
    }

    public void LeavePuzzleView()
    {
        //(PuzzleManager.instance.activePuzzle != null)
//LeavePuzzleView(PuzzleManager.instance.activePuzzle);
    }

}
