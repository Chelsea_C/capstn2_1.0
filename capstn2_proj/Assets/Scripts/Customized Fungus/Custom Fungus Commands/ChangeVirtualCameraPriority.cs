﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using Cinemachine;

/// <summary>
/// Changes the priority of the Virtual Camera
/// </summary>
[CommandInfo ("Cinemachine",
    "Change Virtual Camera Priority",
    "Change the priority of the given virtual camera.")]
//[AddComponentMenu("")]
[ExecuteInEditMode]
public class ChangeVirtualCameraPriority : Command
{
    [Tooltip("The gameObject with a Virtual Camera to be edited.")]
    [SerializeField] protected GameObject _vCamObject;

    [Tooltip("The priority to be set to.")]
    [SerializeField] protected int _priority;


    #region Public Members
    public override void OnEnter()
    {
        if(_vCamObject != null)
        {
            CinemachineVirtualCamera vCam = _vCamObject.GetComponent<CinemachineVirtualCamera>();
            vCam.Priority = _priority;
        }

        Continue();
    }

    public override string GetSummary()
    {
        if(_vCamObject.GetComponent<CinemachineVirtualCamera>() == null)
        {
            return "Error: No virtual camera found on gameObject.";
        }

        return _vCamObject.name;
    }

    public override Color GetButtonColor()
    {
        return new Color32(221, 184, 169, 255);
    }
    #endregion

}
