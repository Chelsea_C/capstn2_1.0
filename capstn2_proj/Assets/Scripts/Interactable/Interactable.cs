﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Fungus;

public class Interactable : MonoBehaviour
{
    [SerializeField] private InteractableData _data;
    //[SerializeField] private Texture2D _handCursor;

    [SerializeField] private SortingLayer _spriteLayer;
    private InteractableLabel _label;
    private DetectPlayer _detector;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    
    protected DetectPlayer detector { get { return _detector; } }
    protected SpriteRenderer spriteRenderer { get { return _spriteRenderer; } set { _spriteRenderer = value; } }

    [Header("Tutorial")]
    public bool isTutorial = false;

    public InteractableData data { get { return _data; } set { _data = value; } }
    

    protected void Start()
    {
        _detector = GetComponent<DetectPlayer>();
        _label = GetComponentInChildren<InteractableLabel>();

        if(_spriteRenderer == null)
            _spriteRenderer = GetComponent<SpriteRenderer>();

        if (_spriteRenderer == null)
            _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        
    }
    

    public Interactable SetActive(Interactable interactable)
    {
        return this;
    }
    
    protected void OnMouseEnter()
    {
        var eventDispatcher = FungusManager.Instance.EventDispatcher;
        eventDispatcher.Raise(new InteractableHover.InteractableHoverEvent(this));
    }

    protected void OnMouseExit()
    {
        FungusManager.Instance.EventDispatcher.Raise(new ExitInteractableHover.ExitInteractableHoverEvent());
    }

    //protected void OnMouseEnter()
    //{
    //    if (detector == null)
    //        return;

    //    if (_detector.playerIsDetected)
    //    {
    //        FindObjectOfType<PlayerActor>().GetComponentInChildren<TextMeshProUGUI>().text = _data.labelText;
    //        Cursor.SetCursor(_handCursor, Vector2.zero, CursorMode.Auto);
    //    }
    //}

    //protected void OnMouseExit()
    //{
    //    if (detector == null)
    //        return;

    //    FindObjectOfType<PlayerActor>().GetComponentInChildren<TextMeshProUGUI>().text = "";
    //    Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    //}
}
