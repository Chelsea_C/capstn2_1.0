﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;



/// <summary>
/// Sets a Pickup component to allow / disallow being able to be picked up.
/// </summary>
[CommandInfo("Interactable",
    "SetAllowPickup",
    "Sets a Pickup component to allow / disallow being able to be picked up.")]
[AddComponentMenu("")]
public class SetAllowPickup : Command
{
    [Tooltip("Reference to Pickup component on a gameObject")]
    [SerializeField] protected Pickup _targetPickup;

    [Tooltip("Set true to allow the object to be picked up")]
    [SerializeField] protected BooleanData _allowPickup;

    #region Public Members
    public override void OnEnter()
    {
        if(_targetPickup != null)
        {
            _targetPickup.isAllowPickup = _allowPickup.Value;
        }

        Continue();
    }

    public override string GetSummary()
    {
        if (_targetPickup == null)
        {
            return "Error: No Pickup component selected";
        }

        return _targetPickup.gameObject.name;
    }

    public override Color GetButtonColor()
    {
        return new Color32(235, 191, 217, 255);
    }

    public override bool HasReference(Variable variable)
    {
        return _allowPickup.booleanRef == variable || base.HasReference(variable);
    }

    #endregion

}
