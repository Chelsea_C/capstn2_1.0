﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableHolder : MonoBehaviour
{
    [SerializeField] private InteractableData _data;
    [SerializeField] private Interactable _interactable;
    [SerializeField] private bool _isAllowItemPlacement = true;
    [SerializeField] private bool _isAllowItemRetrieval = true;
    [SerializeField] private bool _isEmpty = true;

    [SerializeField] private SpriteRenderer _item;
    [SerializeField] private SpriteRenderer _slot;

    public Interactable currentInteractable { get { return _interactable; } set { _interactable = value; } }
    public bool isAllowItemPlacement { get { return _isAllowItemPlacement; } set { _isAllowItemPlacement = value; } }
    public bool isAllowItemRetrieval { get { return _isAllowItemRetrieval; } set { _isAllowItemRetrieval = value; } }

    private void Start()
    {

    }

    private void Update()
    {
        _isEmpty = (_interactable == null) ? true : false;
    }

    private void UpdateSprite()
    {
        if (_data != null)
        {
            _item.sprite = _data.icon;
            _item.gameObject.transform.localScale = _data.iconScale;
        }

        if (_item.gameObject.GetComponent<PolygonCollider2D>() == null)
        {
            _item.gameObject.AddComponent<PolygonCollider2D>();
        }
        else
        {
            Destroy(_item.gameObject.GetComponent<PolygonCollider2D>());
            _item.gameObject.AddComponent<PolygonCollider2D>();
        }
    }

    private void InsertInteractable(Interactable interactable)
    {
        if(!_isAllowItemPlacement)
        {
            return;
        }

    }

    private void RemoveInteractable()
    {
        if(!_isAllowItemRetrieval)
        {
            return;
        }
    }

    private void OnMouseDown()
    {
        Debug.Log("Clicking slot");
    }

    public void PlaceItem(Interactable interactable)
    {
        InsertInteractable(interactable);
    }

    public void TakeItem(Interactable interactable)
    {
        RemoveInteractable();
    }

}
