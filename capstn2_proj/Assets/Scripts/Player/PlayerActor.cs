﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerBounds))]
public class PlayerActor : MonoBehaviour
{
    [SerializeField] private float _playerSpeed;

    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private bool _isSpriteFacingLeft;
    [SerializeField] private Animator _animator;

    [SerializeField] private bool _isUsingKeyboard = false;

    public float playerSpeed { get { return _playerSpeed; } set { _playerSpeed = value; } }

    [SerializeField] private float startTime;
    [SerializeField] private Vector3 _targetPos;
    [SerializeField] private Vector3 _initPos;

    public Vector3 targetPos { get { return _targetPos; } set { _targetPos = value; } }

    private PlayerBounds _bounds;
    private bool _isMoving;

    private void Start()
    {
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        _initPos = transform.position;
        _targetPos = transform.position;
        _bounds = GetComponent<PlayerBounds>();
        _animator = GetComponentInChildren<Animator>();
    }
    
    private void Update()
    {
        Move();
        Animate();
    }

    private void Move()
    {
        #region Keyboard

        if (_isUsingKeyboard)
        {
            // Move right
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                transform.Translate(Vector3.right * _playerSpeed * Time.deltaTime, Camera.main.transform);
            }

            // Move left
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                transform.Translate(Vector3.left * _playerSpeed * Time.deltaTime, Camera.main.transform);
            }
        }
        #endregion

        #region Mouse

        if (Input.GetMouseButtonDown(0))
        {
            startTime = Time.time;
            _initPos = transform.position;
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            _targetPos = new Vector3(mousePos.x, transform.position.y, transform.position.z);

                
            if (mousePos.x < transform.position.x)
            {
                //Debug.Log("Moving left: " + mousePos.x);
                UpdateSprite(true);
            }

            if(mousePos.x >= transform.position.x)
            {
                //Debug.Log("Moving right: " + mousePos.x);
                UpdateSprite(false);
            }
        }
        #endregion

        if (_targetPos.x > _bounds.xMax || _targetPos.x < _bounds.xMin)
        {
            _isMoving = false;
            transform.position = _initPos;
            return;
        }

        _isMoving = (transform.position.x == _targetPos.x) ? false : true;
        float step = _playerSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, _targetPos, step);
        

    }

    private void UpdateSprite(bool isMovingLeft)
    {
        if(isMovingLeft)
        {
            if (_isSpriteFacingLeft)
                _spriteRenderer.flipX = false;
            else
                _spriteRenderer.flipX = true;
        }
        else
        {
            if (_isSpriteFacingLeft)
                _spriteRenderer.flipX = true;
            else
                _spriteRenderer.flipX = false;
        }
    }

    private void Animate()
    {
        _animator.SetBool("isWalking", _isMoving);
    }
}
