﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    [SerializeField] private List<PuzzleStep> _steps;
    private PuzzleStep _currentStep;

    private int _currentIndex;
    public int currentIndex { get { return _currentIndex; } set { _currentIndex = value; } }


    private void Awake()
    {
        _steps = new List<PuzzleStep>();
    }


    private void Start()
    {
        _currentIndex = 0;
        GetSteps();
        _currentStep = _steps[_currentIndex];
    }


    private void Update()
    {

        if (_currentStep != null)
            _currentStep.CheckIfHappening();
    }


    private void SetNextStep(int currentOrder)
    {
        _currentStep = GetStepByOrder(currentOrder);

        if (_currentStep == null)
        {
            CompletedAllTasks();
            return;
        }
    }


    private PuzzleStep GetStepByOrder(int index)
    {
        if (index < _steps.Count)
            return _steps[index];
        return null;
    }


    private void GetSteps()
    {
        _steps = new List<PuzzleStep>();
        PuzzleStep[] _puzzleSteps = GetComponentsInChildren<PuzzleStep>();
        for (int i = 0; i < _puzzleSteps.Length; i++)
        {
            _steps.Add(_puzzleSteps[i]);
        }
    }


    public void CompletedTask()
    {
        _currentIndex++;
        SetNextStep(_currentIndex);
    }


    public virtual void CompletedAllTasks()
    {
        Debug.Log(name + ": Completed all tasks");
    }
}
