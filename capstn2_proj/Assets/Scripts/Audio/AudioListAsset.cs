﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AudioListAsset : ScriptableObject
{
    [SerializeField] private AudioClip[] _clips;

}
