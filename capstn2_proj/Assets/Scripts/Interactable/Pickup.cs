﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


[RequireComponent(typeof(Collider), typeof(SpriteRenderer))]
public class Pickup : Interactable
{

    [SerializeField] private Vector3 _scale = new Vector3(1,1,1);
    [SerializeField] private float _scaleFactor = 1.0f;
    [SerializeField] private bool _isAllowPickup;
    [SerializeField] private bool _isInPlayerRange;

    public bool isAllowPickup { get { return _isAllowPickup; } set { _isAllowPickup = value; } }

    private new void Start()
    {
        base.Start();
        spriteRenderer.sprite = data.icon;
        spriteRenderer.gameObject.transform.localScale = _scale * _scaleFactor;
    }

    private void Update()
    {
        spriteRenderer.sprite = data.icon;
    }

    public void SetPickup(bool set)
    {
        isAllowPickup = set;
    }

    public void OnMouseDown()
    {
        if (!_isAllowPickup)
                    return;

        var eventDispatcher = Fungus.FungusManager.Instance.EventDispatcher;
        eventDispatcher.Raise (new PickUpInteractable.PickUpInteractableEvent(this));
    }
}
