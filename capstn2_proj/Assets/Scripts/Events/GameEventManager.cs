﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventManager : MonoBehaviour
{
    public static GameEventManager instance;

    private void Awake()
    {
        instance = this;   
    }

    public static event Action<GameEvent> OnPuzzleCompleted = delegate { };
    public static event Action<GameEvent> OnItemPickup = delegate { };
    public static event Action<GameEvent> OnPlayerDetected = delegate { };

    private void Start()
    {
        
    }
    
    void Update()
    {
        
    }
}
