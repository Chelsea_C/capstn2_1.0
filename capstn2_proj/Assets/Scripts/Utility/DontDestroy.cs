﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    [SerializeField] private string _tag;

    private void Awake()
    {
        if (_tag.Length > 0)
        {
            GameObject[] obs = GameObject.FindGameObjectsWithTag(_tag);
            if (obs.Length > 1)
                Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

    }

}
