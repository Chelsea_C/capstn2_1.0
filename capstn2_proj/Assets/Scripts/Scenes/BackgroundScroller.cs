﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    public enum DIRECTION
    {
        UP = 1,
        DOWN = 2,
        LEFT = 3,
        RIGHT = 4
    }

    [SerializeField] private float _scrollSpeed;
    [SerializeField] private float _tileSizeY;
    [SerializeField] private float _tileSizeX;
    [SerializeField] private DIRECTION scrollDirection = DIRECTION.DOWN;
    [SerializeField] private Collider2D _spriteSizeReference;
    [SerializeField] private int _spritesPerTile;

    private Vector3 _startPosition;
    private float _tileSize;
    private Vector3 _direction;

    private void Start()
    {
        _tileSizeX = _spriteSizeReference.bounds.size.x * _spritesPerTile;
        _tileSizeY = _spriteSizeReference.bounds.size.y * _spritesPerTile;

        _startPosition = transform.position;

        switch (scrollDirection)
        {
            case DIRECTION.DOWN:
                _tileSize = _tileSizeY;
                _direction = Vector3.down;
                break;
            case DIRECTION.UP:
                _tileSize = _tileSizeY;
                _direction = Vector3.up;
                break;
            case DIRECTION.LEFT:
                _tileSize = _tileSizeX;
                _direction = Vector3.left;
                break;
            case DIRECTION.RIGHT:
                _tileSize = _tileSizeX;
                _direction = Vector3.right;
                break;
            default:
                break;
        }
    }

    private void Update()
    {
        Scroll();
    }

    private void Scroll()
    {        

        float newPosition = Mathf.Repeat(Time.time * _scrollSpeed, _tileSize);
        transform.position = (_startPosition + _direction * newPosition);
    }

}
