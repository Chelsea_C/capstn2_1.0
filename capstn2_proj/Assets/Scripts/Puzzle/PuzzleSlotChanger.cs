﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSlotChanger : MonoBehaviour
{
    [SerializeField] private PuzzleSlot _slot;
    public PuzzleSlot slot { get { return _slot; } }

    [SerializeField] private bool _loadsNextSprite = true;
    private bool trigger = true;

    private MatchSlots _puzzle;

    private void Start()
    {
        _slot = GetComponentInParent<PuzzleSlot>();
        _puzzle = transform.parent.parent.GetComponentInChildren<MatchSlots>();
    }

    public void OnMouseDown()
    {
        if (trigger && !_slot.isLocked)
        {
            trigger = false;
            if (_loadsNextSprite)
            {
                slot.LoadNextSprite();
            }
            else
            {
                slot.LoadPreviousSprite();
            }
            trigger = true;
        }
    }

}
