﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private Collider2D _boundary;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private bool _isFollowingPlayer;

    [SerializeField] private float distanceFromMinBoundary;
    [SerializeField] private float distanceFromMaxBoundary;

    public float boundaryMaxX;
    public float boundaryMinX;
    public float camHalfWidth;

    
    //public bool isFollowingPlayer { get { return _isFollowingPlayer; } set { _isFollowingPlayer = value; } }

    void Start()
    {
        _offset = transform.position - _player.transform.position;
    }
    
    void Update()
    {
        camHalfWidth = ((float) Screen.width / (float) Screen.height) * Camera.main.orthographicSize;
        float playerX = _player.transform.position.x;
        boundaryMaxX = _boundary.bounds.max.x;
        boundaryMinX = _boundary.bounds.min.x;

        distanceFromMinBoundary = playerX - (boundaryMinX + camHalfWidth);
        distanceFromMaxBoundary = playerX - (boundaryMaxX - camHalfWidth);
        // If player is at least half a camera's width from the map boundaries
        // if (playerX < boundaryMaxX - camHalfWidth || playerX > boundaryMinX + camHalfWidth)
        if(distanceFromMaxBoundary >= 0 || distanceFromMinBoundary >= 0)
        {

            //Debug.Log("Distance from edge: " + Mathf.Abs(playerX - (boundaryMinX + camHalfWidth)));
            _isFollowingPlayer = true;
            transform.position = _player.transform.position + _offset;
        }
        // If player is within half a camera's width of the map boundaries
        else
        {
            //Debug.Log("Player: EDGE");
            _isFollowingPlayer = false;
            transform.position = transform.position;
        }

    }
}
